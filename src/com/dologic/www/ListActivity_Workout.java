package com.dologic.www;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ListActivity_Workout extends Activity {

	// Initialize the arrays
	String[] bodyPartArray = { "Chest", "Back", "Legs", "Shoulders"};

	// Declare the UI components
	private ListView bodyPartListView;

	@SuppressWarnings("rawtypes")
	private ArrayAdapter arrayAdapter;

	/** Called when the activity is first created. */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listview_workout);

		// Initialize the UI components
		bodyPartListView = (ListView) findViewById(R.id.Workout_list);
		
		// For this moment, you have ListView where you can display a list.
		// But how can we put this data set to the list?
		// This is where you need an Adapter

		// context - The current context.
		// resource - The resource ID for a layout file containing a layout 
		// to use when instantiating views.
		// From the third parameter, you plugged the data set to adapter
		arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, bodyPartArray);

		// By using setAdapter method, you plugged the ListView with adapter
		bodyPartListView.setAdapter(arrayAdapter);
		
		// On ListView Item Click
		bodyPartListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
		    public void onItemClick(AdapterView parent, View v, int position, long id){
		      
		    	// Retrieve Item Text
		    	String text = (String) ((TextView)v).getText();

		    	// Set TextView in MainActivity
		    	//MainActivity.mText.setText(text);
		    	
		    	Intent i = new Intent(); // Define a new intent
		    	i.putExtra("stringData", text); // Pass the extra data
		    	setResult(RESULT_OK, i); // Result code, choose RESULT_OK or RESULT_CANCELED
		    	finish(); // Closes the activity and returns to the caller activity
		    }
		});
			
		}
	
}
