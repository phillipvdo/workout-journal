package com.dologic.www;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.EditText;

public class MainActivity extends Activity {

	private TextView mText, mText2, mText3, mText4, mText5;
	public static String KEY_NAME = "NAME";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mText = (TextView)findViewById(R.id.textView_lblWorkoutSelect);
		mText2 = (TextView)findViewById(R.id.textView_lblExerciseSelect);
		//mText3 = (TextView)findViewById(R.id.textView_lblSet);
		//mText4 = (TextView)findViewById(R.id.textView_lblRep);
		//mText5 = (TextView)findViewById(R.id.textView_lblWeight);

		mText.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				Intent i = new Intent(MainActivity.this, ListActivity_Workout.class); // Start new intent, main activity to second activity
				final int result = 1; // Set the integer identifier used as correlation id to identify which sub activity has finished
				startActivityForResult(i, result); // Start the second activity, and expect data to be returned to the main activity
			}
		});

		mText2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				
				String WorkoutSelectData = new String();
				WorkoutSelectData = ((TextView)findViewById(R.id.textView_lblWorkoutSelect)).getText().toString();

				KEY_NAME = WorkoutSelectData;
				Bundle bundleData = new Bundle();
				bundleData.putString(KEY_NAME, WorkoutSelectData);
				
				Intent i = new Intent(MainActivity.this, ListActivity_Exercises.class); // Start new intent, main activity to second activity
				final int result = 2; // Set the integer identifier used as correlation id to identify which sub activity has finished
				
				i.putExtras(bundleData);
				startActivityForResult(i, result); // Start the second activity, and expect data to be returned to the main activity
			}
		});
		

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);

		if(data != null){ // If the user does not make a selection, and presses the back button, this prevents program crash
			String extraData=data.getStringExtra("stringData");

			if(requestCode == 1){
				mText.setText(" " + extraData);
			} else if(requestCode == 2){
				mText2.setText(" " + extraData);
			}
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
}


