package com.dologic.www;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ListActivity_Exercises extends Activity {

	private String WorkoutSelectData;

	// Initialize the arrays
	String[] chestArray = {"Incline Bench Press", "Dumbell Bench Press", "Incline Flys", "Dips", "Bench Press"};
	String[] backArray = {"Deadlifts", "Pull-Ups","Cable Pulldowns", "Seated Cable Rows", "Face Pulls"};
	String[] legsArray = {"Squats", "Leg Press", "Calf Press"};
	String[] shouldersArray = {"Dumbell Shoulder Press", "Dumbell Rear Latereal Raises", "Shoulder Side Raise", "Barbell Shoulder Press", "External Rotator", "Internal Rotator"};
	String[] variableArray;

	// Declare the UI components
	private ListView chestListView, backListView, legsListView, shouldersListView;
	private ListView variableListView;

	@SuppressWarnings("rawtypes")
	private ArrayAdapter arrayAdapter;

	/** Called when the activity is first created. */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listview_workout);
		
		Bundle bundleData = getIntent().getExtras();
		String WorkoutSelectData = bundleData.getString(MainActivity.KEY_NAME);

		if (WorkoutSelectData.equals(" Chest")){
			variableListView = chestListView;
			variableArray = chestArray;
		} else if (WorkoutSelectData.equals(" Back")){
			variableListView = backListView;
			variableArray = backArray;
		} else if (WorkoutSelectData.equals(" Legs")){
			variableListView = legsListView;
			variableArray = legsArray;
		} else if (WorkoutSelectData.equals(" Shoulders")){
			variableListView = shouldersListView;
			variableArray = shouldersArray;
		} else{
			//variableListView = variableListView;
			//variableArray = variableArray;
		}

		variableListView = (ListView) findViewById(R.id.Workout_list);// Initialize the UI components
		arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, variableArray);// Add the data set to he list
		variableListView.setAdapter(arrayAdapter);// By using setAdapter method, you plugged the ListView with adapter

		// On ListView Item Click
		variableListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView parent, View v, int position, long id){

				// Retrieve Item Text
				String text = (String) ((TextView)v).getText();

				Intent output = new Intent(); // Define a new intent
				output.putExtra("stringData", text); // Pass the extra data
				setResult(RESULT_OK, output); // Result code, choose RESULT_OK or RESULT_CANCELED
				finish(); // Closes the activity and returns to the caller activity  
			}
		});

	}

	public String getWorkoutSelectData() {
		
		return WorkoutSelectData;
	}
	public void setWorkoutSelectData(String workoutSelectData) {
		
		WorkoutSelectData = workoutSelectData;
	}
}

